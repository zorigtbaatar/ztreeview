﻿using System;
using System.Drawing;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Windows.Forms;

namespace ZTreeView
{
    public delegate void OnAddItem(TreeItem newItem);

    public delegate void OnDeleteItem(TreeItem deletedItem);

    public partial class TreeView : Panel
    {
        private TreeItem rootItem;
        private ImageList list;

        public event OnAddItem onAddItem;

        public event OnDeleteItem onDeleteItem;

        public TreeView(TreeItem item, ImageList list)
        {
            InitializeComponent();
            this.rootItem = item;
            this.list = list;
            initializeTreeView();
            
        }

        private void initializeTreeView()
        {
            treeView1.Nodes.Clear();
            if(list != null)
                initImage();
            

            
            addItemsToTree(rootItem, null);

            treeView1.ExpandAll();
        }

        

        private void onSelectTreeNode(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeItem selectedItem = (TreeItem)e.Node.Tag;
            showItemActionMenu(selectedItem, e);
        }

        private void showItemActionMenu(TreeItem selectedItem, TreeNodeMouseClickEventArgs e)
        {
            ContextMenuStrip contextMenuStrip = new ContextMenuStrip();

            ResourceManager rm = new ResourceManager("ZTreeView.Resources.buttonLogo", Assembly.GetExecutingAssembly());

            Image addIcon = (Image)rm.GetObject("add");
            Image deleteIcon = (Image)rm.GetObject("del");

            ToolStripMenuItem addMenu = new ToolStripMenuItem(addIcon);
            addMenu.Click += (s, e) => addItem(selectedItem);

            ToolStripMenuItem deleteMenu = new ToolStripMenuItem(deleteIcon);
            deleteMenu.Click += (s, e) => deleteItem(selectedItem);

            contextMenuStrip.Items.Add(addMenu);
            contextMenuStrip.Items.Add(deleteMenu);

            this.ContextMenuStrip = contextMenuStrip;
            this.Update();

            contextMenuStrip.Show(treeView1, e.Location.X + 40, e.Location.Y - 18);
        }

        private void addItem(TreeItem parentItem)
        {
            // TODO add new Item to this parent Item and update UI
            addDataDialog data = new addDataDialog();
            TreeItem newCateg = null;

            var result = data.ShowDialog();

            if (result != DialogResult.OK)
                return;

            newCateg = data.temp;

            treeView1.SelectedNode.Nodes.Add(newCateg.name);

            // TODO call item registration handlers...
            onAddItem(newCateg);
        }

        private void deleteItem(TreeItem itemToDelete)
        {
            // TODO delete item and update
            treeView1.SelectedNode.Remove();

            // TODO call item deletion handlers...
            onDeleteItem(itemToDelete);
        }

        private void initImage()
        {
            treeView1.ImageList = list;
            treeView1.ImageIndex = 0;
            treeView1.SelectedImageIndex = 1;
        }

        private void addItemsToTree(TreeItem item, TreeNode parentNode)
        {
            if (item == null)
                return;

            TreeNode childNode = new TreeNode(item.name + "-" + item.level);

            childNode.Tag = item;
            if (parentNode == null)
            {
                parentNode = childNode;
                treeView1.Nodes.Add(parentNode);
            }
            else
                parentNode.Nodes.Add(childNode);

            if (item.children.Count == 0)
                return;

            foreach (TreeItem child in item.children)
                addItemsToTree(child, childNode);
        }
    }
}