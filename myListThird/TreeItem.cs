﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZTreeView
{
   public class TreeItem
    {
        public string name { get; set; }
        public int level { get;set; }
        public object id { get; }

        public List<TreeItem> children = new List<TreeItem>();

        public TreeItem(string name)
        {
            this.name = name;
        }

        public TreeItem(string name,int level)
        {
            this.name = name;
            this.level = level;
        }

        public TreeItem(object id,string name, int level)
        {
            this.id = id;
            this.name = name;
            this.level = level;
        }


        public void addChild(TreeItem child)
        {
            children.Add(child);
        }

    }
}
