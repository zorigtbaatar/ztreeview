﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZTreeView
{
    public partial class addDataDialog : Form
    {
        public addDataDialog()
        {
            InitializeComponent();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        public TreeItem temp { get; set; }

        private void addBtn_Click(object sender, EventArgs e)
        {
            TreeItem categ = new TreeItem(textBox1.Text);
            this.temp = categ;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
