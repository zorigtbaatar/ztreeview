using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Reflection;
using System.Threading;

namespace ZTreeView
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ResourceManager rm = new ResourceManager("ZTreeView.Resources.buttonLogo", Assembly.GetExecutingAssembly());

            rm.GetObject("del");
            Console.WriteLine(Thread.CurrentThread.CurrentCulture.Name);

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        }
    }
}
