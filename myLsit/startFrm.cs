﻿using ZTreeView;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Reflection;

namespace myLsit
{
    public partial class startFrm : Form
    {
        public startFrm()
        {
            InitializeComponent();
            initData();
        }

        private void initData()
        {
            TreeItem rootItem = new TreeItem("Дэлхий", 0);
            TreeItem asia = new TreeItem("Ази", 1);
            TreeItem eu = new TreeItem("Ёвроп", 1);
            TreeItem af = new TreeItem("Африк", 2);
            TreeItem au = new TreeItem("Австрали", 3);
            TreeItem namerica = new TreeItem("Хойт Америк", 3);
            TreeItem samerica = new TreeItem("Өмнөд Америк", 3);

            TreeItem mn = new TreeItem("Монгол", 3);
            TreeItem ch = new TreeItem("Хятад", 3);
            TreeItem jp = new TreeItem("Япон", 3);
            TreeItem kr = new TreeItem("Солонгос", 3);
            TreeItem ind = new TreeItem("Энэтхэг", 3);

            TreeItem uk = new TreeItem("Их Британи", 3);
            TreeItem fr = new TreeItem("Франц", 3);
            TreeItem sp = new TreeItem("Спани", 3);
            TreeItem it = new TreeItem("Итали", 3);
            TreeItem gr = new TreeItem("Грээк", 3);

            TreeItem eg = new TreeItem("Эгипт", 3);
            TreeItem lv = new TreeItem("Ливи", 3);
            TreeItem ng = new TreeItem("Нигэр", 3);
            TreeItem sudan = new TreeItem("Судан", 3);
            TreeItem etiop = new TreeItem("Этопи", 3);

            TreeItem usa = new TreeItem("АНУ", 3);
            TreeItem canada = new TreeItem("Канад", 3);

            TreeItem cl = new TreeItem("Чили", 3);
            TreeItem bz = new TreeItem("Бразил", 3);
            TreeItem cm = new TreeItem("Колумб", 3);


            asia.addChild(mn);
            asia.addChild(ch);
            asia.addChild(jp);
            asia.addChild(kr);
            asia.addChild(ind);

            eu.addChild(uk);
            eu.addChild(fr);
            eu.addChild(sp);
            eu.addChild(it);
            eu.addChild(gr);

            af.addChild(eg);
            af.addChild(lv);
            af.addChild(ng);
            af.addChild(sudan);
            af.addChild(etiop);

            namerica.addChild(usa);
            namerica.addChild(canada);

            samerica.addChild(cl);
            samerica.addChild(bz);
            samerica.addChild(cm);


            rootItem.addChild(asia);
            rootItem.addChild(eu);
            rootItem.addChild(af);
            rootItem.addChild(au);
            rootItem.addChild(namerica);
            rootItem.addChild(samerica);

            //init images
            ImageList myImageList = new ImageList();
            ResourceManager rm = new ResourceManager("start.resource.images", Assembly.GetExecutingAssembly());

            myImageList.Images.Add((Image)rm.GetObject("modern"));
            myImageList.Images.Add((Image)rm.GetObject("ocean"));
            myImageList.Images.Add((Image)rm.GetObject("sing"));
            myImageList.Images.Add((Image)rm.GetObject("storm"));
            myImageList.Images.Add((Image)rm.GetObject("wave"));

            ZTreeView.TreeView tree = new ZTreeView.TreeView(rootItem, myImageList);
            tree.onAddItem += onAddItem;
            tree.onDeleteItem += onDeleteItem;

            Controls.Add(tree);

        }

        private void onAddItem(TreeItem newItem)
        {

        }

        private void onDeleteItem(TreeItem item)
        {

        }
    }
}
